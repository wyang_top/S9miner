#! /bin/sh

NUMBER_THREADS=`cat /proc/cpuinfo | grep "processor" | wc -l`

tar xvf u-boot-xlnx-xilinx-v2018.3.tar.gz
tar xvf uboot_patch.tar.bz2 -C ./u-boot-xlnx-xilinx-v2018.3
cd u-boot-xlnx-xilinx-v2018.3
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zynq_s9miner_config
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j${NUMBER_THREADS}
cp ./u-boot ../
mv ../u-boot  ../u-boot.elf