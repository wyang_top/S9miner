#! /bin/sh


#从github上下载linux-xlnx-xilinx-v2018.3.tar.gz
if [ -f "./linux-xlnx-xilinx-v2018.3.tar.gz" ];then
  echo "linux-xlnx-xilinx-v2018.3.tar.gz"
  else
  wget https://codeload.github.com/Xilinx/linux-xlnx/tar.gz/xilinx-v2018.3
  mv xilinx-v2018.3 linux-xlnx-xilinx-v2018.3.tar.gz
fi

tar xvf linux-xlnx-xilinx-v2018.3.tar.gz

NUMBER_THREADS=`cat /proc/cpuinfo | grep "processor" | wc -l`

cd linux-xlnx-xilinx-v2018.3
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- xilinx_zynq_defconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- prepare scripts
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- UIMAGE_LOADADDR=0x8000 uImage -j${NUMBER_THREADS}
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- modules -j${NUMBER_THREADS}
cp arch/arm/boot/uImage ../